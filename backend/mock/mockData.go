package mock

import "contractor-scheduler/models"

var MockSchedules = models.ScheduleResponse{
	Schedules: []*models.Schedule{
		{
			Appointments: []*models.Appointment{
				{
					Start:        "9:00 AM",
					End:          "11:00 AM",
					CustomerName: "Tejas",
					CustomerLocation: models.Location{
						Latitude:  18.5512,
						Longitude: 73.7898,
					},
					Trades: "heating",
				},
				{
					Start:        "3:00 PM",
					End:          "5:00 PM",
					CustomerName: "Raju",
					CustomerLocation: models.Location{
						Latitude:  18.7041,
						Longitude: 73.7564,
					},
					Trades: "heating",
				},
			},
		},
		{
			Name: "Vishal",
			Appointments: []*models.Appointment{
				{
					Start:        "11:00 AM",
					End:          "2:00 PM",
					CustomerName: "Bhumika",
					CustomerLocation: models.Location{
						Latitude:  18.5292,
						Longitude: 73.8740,
					},
					Trades: "appliances",
				},
			},
		},
		{
			Name: "Deepak",
			Appointments: []*models.Appointment{
				{
					Start:        "11:00 AM",
					End:          "2:00 PM",
					CustomerName: "Gaurav",
					CustomerLocation: models.Location{
						Latitude:  18.5942,
						Longitude: 73.7919,
					},
					Trades: "air-conditioning",
				},
			},
		},
	},
}
