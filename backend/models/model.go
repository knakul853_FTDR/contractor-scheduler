package models

import "time"

// Define data structures
type Contractor struct {
	Name         string       `json:"name"`
	Trades       []string     `json:"trades"`
	Latitude     float64      `json:"latitude"`
	Longitude    float64      `json:"longitude"`
	Availability []TimeWindow `json:"availability"`
	Address      string       `json:"address"`
}

type Customer struct {
	Name       string     `json:"name"`
	Trade      string     `json:"trade"`
	TimeWindow TimeWindow `json:"timeWindow"`
	Latitude   float64    `json:"latitude"`
	Longitude  float64    `json:"longitude"`
	Address    string     `json:"address"`
	ItemName   string     `json:"itemName"`
}

type TimeWindow struct {
	Start time.Time `json:"start"`
	End   time.Time `json:"end"`
}

type ScheduleResponse struct {
	Schedules []*Schedule `json:"schedules"`
	Error     error       `json:"error"`
}

type Schedule struct {
	Name         string         `json:"name"`
	Location     Location       `json:"location"`
	Address      string         `json:"address"`
	Appointments []*Appointment `json:"appointments"`
}

type Appointment struct {
	Start            string   `json:"start"`
	End              string   `json:"end"`
	CustomerName     string   `json:"customerName"`
	CustomerLocation Location `json:"customerLocation"`
	CustomerAddress  string   `json:"customerAddress"`
	Trades           string   `json:"trades"`
	ItemName         string   `json:"itemName"`
}

type Location struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}
