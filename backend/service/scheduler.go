package service

import (
	"context"
	"contractor-scheduler/models"
	"math"
	"sort"
)

// ContractorService defines the interface for contractor-related operations
type ContractorService interface {
	GetAvailableContractors(ctx context.Context, customer models.Customer) ([]models.Contractor, error)
	AssignContractors(ctx context.Context, customers []models.Customer, contractors []models.Contractor) (*models.ScheduleResponse, error)
}

// contractorService implements the ContractorService interface
type contractorService struct{}

func NewContractorService() ContractorService {
	return &contractorService{}
}
func (s *contractorService) GetAvailableContractors(ctx context.Context, customer models.Customer) ([]models.Contractor, error) {
	// Implement logic to retrieve available contractors from a data store
	// Return available contractors or an error if something goes wrong
	return nil, nil
}

func (s *contractorService) AssignContractors(ctx context.Context, customers []models.Customer, contractors []models.Contractor) (*models.ScheduleResponse, error) {
	layout := "2006-01-02 15:04:05"
	appointments := make(map[string][]*models.Appointment)

	var schedules []*models.Schedule

	for _, customer := range customers {
		availableContractors := []models.Contractor{}
		for _, contractor := range contractors {
			if isAvailable(contractor, customer.TimeWindow) {
				for _, trade := range contractor.Trades {
					if trade == customer.Trade {
						availableContractors = append(availableContractors, contractor)
						break
					}
				}
			}
		}

		if len(availableContractors) > 0 {
			sort.Slice(availableContractors, func(i, j int) bool {
				dist1 := haversine(customer.Latitude, customer.Longitude, availableContractors[i].Latitude, availableContractors[i].Longitude)
				dist2 := haversine(customer.Latitude, customer.Longitude, availableContractors[j].Latitude, availableContractors[j].Longitude)
				return dist1 < dist2
			})

			for i, contractor := range contractors {
				if contractor.Name == availableContractors[0].Name {

					contractors[i].Availability = append(contractors[i].Availability, customer.TimeWindow)

					appointments[contractor.Name] = append(appointments[contractor.Name], &models.Appointment{
						Start:        customer.TimeWindow.Start.Format(layout),
						End:          customer.TimeWindow.End.Format(layout),
						CustomerName: customer.Name,
						ItemName:     customer.ItemName,
						CustomerLocation: models.Location{
							Latitude:  customer.Latitude,
							Longitude: customer.Longitude,
						},
						CustomerAddress: customer.Address,
						Trades:          customer.Trade,
					})

					break
				}
			}
		}
	}

	for _, contractor := range contractors {

		if appointment, ok := appointments[contractor.Name]; ok {

			schedules = append(schedules, &models.Schedule{
				Address:      contractor.Address,
				Name:         contractor.Name,
				Appointments: appointment,
				Location: models.Location{
					Latitude:  contractor.Latitude,
					Longitude: contractor.Longitude,
				},
			})
		}
	}

	return &models.ScheduleResponse{
		Schedules: schedules,
		Error:     nil,
	}, nil
}

// Haversine formula to calculate the distance between two points on the Earth
func haversine(lat1, lon1, lat2, lon2 float64) float64 {
	const R = 6371 // Earth radius in kilometers
	dLat := (lat2 - lat1) * (math.Pi / 180)
	dLon := (lon2 - lon1) * (math.Pi / 180)
	a := math.Sin(dLat/2)*math.Sin(dLat/2) + math.Cos(lat1*(math.Pi/180))*math.Cos(lat2*(math.Pi/180))*math.Sin(dLon/2)*math.Sin(dLon/2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	return R * c
}

// Check if a contractor is available during the specified time window
func isAvailable(contractor models.Contractor, window models.TimeWindow) bool {
	for _, availability := range contractor.Availability {
		if window.Start.After(availability.End) || window.End.Before(availability.Start) || window.Start.Equal(availability.End) {
			continue
		}
		return false
	}
	return true
}
