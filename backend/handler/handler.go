package handler

import (
	"context"
	"contractor-scheduler/models"
	"contractor-scheduler/service"
	"encoding/json"
	"net/http"
	"time"
)

func getMockData() ([]models.Contractor, []models.Customer) {

	var Contractors []models.Contractor
	var Customers []models.Customer

	Contractors = append(Contractors, models.Contractor{Name: "Mr John Sinha",
		Trades:    []string{"heating", "air-conditioning"},
		Latitude:  40.7128,
		Longitude: -74.0060,
		Address:   "123 Main St, New York, NY 10001, USA",
	})
	Contractors = append(Contractors, models.Contractor{Name: "Mr Narendra Smith",
		Trades:    []string{"heating"},
		Latitude:  34.0522,
		Longitude: -118.2437,
		Address:   "456 Elm St, Los Angeles, CA 90001, USA",
	})
	Contractors = append(Contractors, models.Contractor{Name: "Mr Johny Depp",
		Trades:    []string{"appliances"},
		Latitude:  29.7604,
		Longitude: -95.3698,
		Address:   "456, Main Street, Houston, TX 77002, USA",
	})
	Contractors = append(Contractors, models.Contractor{Name: "Mr Tom Cruise",
		Trades:    []string{"PSC"},
		Latitude:  41.8781,
		Longitude: -87.6298,
		Address:   "101 Oak St, Chicago, IL 60601, USA",
	})

	Customers = []models.Customer{
		{Name: "Mr John Smith",
			Trade:      "heating",
			TimeWindow: models.TimeWindow{Start: parseTimeOrPanic("2024-06-10 09:00:00"), End: parseTimeOrPanic("2024-06-10 11:00:00")},
			Latitude:   25.7617,
			Longitude:  -80.1918,
			Address:    "101, Ocean Drive, Miami, FL 33139, USA",
			ItemName:   "Air-conditioner",
		},
		{Name: "Mr John Weasly",
			Trade:      "appliances",
			TimeWindow: models.TimeWindow{Start: parseTimeOrPanic("2024-06-10 11:00:00"), End: parseTimeOrPanic("2024-06-10 14:00:00")},
			Latitude:   32.7767,
			Longitude:  -96.7970,
			Address:    "123, Maple Avenue, Dallas, TX 75201, USA",
		},
		{Name: "Mr Adams Palmer",
			Trade:      "air-conditioning",
			TimeWindow: models.TimeWindow{Start: parseTimeOrPanic("2024-06-10 11:01:00"), End: parseTimeOrPanic("2024-06-10 14:00:00")},
			Latitude:   37.7749,
			Longitude:  -122.4194,
			Address:    "132, My Street, San Francisco, CA 94101, USA",
			ItemName:   "AC",
		},
		{Name: "Jeffrey Jhong",
			Trade:      "heating",
			TimeWindow: models.TimeWindow{Start: parseTimeOrPanic("2024-06-10 15:00:00"), End: parseTimeOrPanic("2024-06-10 17:00:00")},
			Latitude:   37.3382,
			Longitude:  -121.8863,
			Address:    "789, First Street, San Jose, CA 95112, USA",
			ItemName:   "Heater",
		},
	}

	return Contractors, Customers
}

func GetSchedules(w http.ResponseWriter, r *http.Request, contractorService service.ContractorService) {
	w.Header().Set("Content-Type", "application/json")

	ctx := context.Background()
	Contractors, Customers := getMockData()
	resp, err := contractorService.AssignContractors(ctx, Customers, Contractors)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(map[string]string{"error": err.Error()})
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func parseTimeOrPanic(timeStr string) time.Time {
	t, err := parseTime(timeStr)
	if err != nil {
		panic(err)
	}
	return t
}

// Parse time string in the format "yyyy-mm-dd hh:mm:ss"
func parseTime(timeStr string) (time.Time, error) {
	layout := "2006-01-02 15:04:05"
	return time.Parse(layout, timeStr)
}
