package main

import (
	"context"
	"contractor-scheduler/handler"
	"contractor-scheduler/service"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {

	// Initialize service
	contractorService := service.NewContractorService()

	// Initialize MongoDB client
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(context.Background())

	r := mux.NewRouter()

	// Setup CORS
	corsOptions := cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		AllowCredentials: true,
	}
	corsHandler := cors.New(corsOptions).Handler(r)

	// Define routes and pass the service
	r.HandleFunc("/contractor-scheduler/schedules", func(w http.ResponseWriter, r *http.Request) {
		handler.GetSchedules(w, r, contractorService)
	}).Methods("GET", "OPTIONS")

	// Start the server
	fmt.Println("Server is running on port 8000")
	log.Fatal(http.ListenAndServe(":8000", corsHandler))
}
