import { useEffect, useState } from "react";
import "./App.css";
import { APIProvider, Map } from "@vis.gl/react-google-maps";
import { Schedule } from "./components/Schedule";
import { getSchedules } from "./services/schedule";
import { getRandomColor } from "./utils/color";
function App() {
  const [isLoading, setIsLoading] = useState(false);
  const [schedules, setSchedules] = useState([]);
  const [selectedSchedule, setSelectedSchedule] = useState(null);
  const [error, setError] = useState();
  //Get the data from server
  useEffect(() => {
    (async function () {
      setIsLoading(true);
      try {
        const schedules = await getSchedules();
        setSchedules(schedules);
        setIsLoading(false);
      } catch (e) {
        setError(e);
        setIsLoading(false);
      }
    })();
  }, []);
  console.log("app");
  return (
    <div className="App">
      <div className="schedule-list">
        <ul>
          <li
            onClick={() => setSelectedSchedule(null)}
            className={selectedSchedule === null && "sechedule-selected"}
          >
            All
          </li>
          {schedules.map((schedule) => {
            return (
              <li
                key={schedule.name}
                onClick={() => setSelectedSchedule(schedule.name)}
                className={
                  selectedSchedule === schedule.name && "sechedule-selected"
                }
              >
                {schedule.name}
              </li>
            );
          })}
        </ul>
      </div>
      <APIProvider apiKey={process.env.REACT_APP_MAP_API_KEY}>
        {isLoading ? (
          <div>Loading....</div>
        ) : error ? (
          <div> Error : {error}</div>
        ) : (
          <Map
            mapId={"bf51a910020fa25a"}
            style={{ width: "100vw", height: "100vh" }}
            defaultCenter={{
              lat: 40.7128,
              lng: -74.006,
            }}
            defaultZoom={15}
            gestureHandling={"greedy"}
            disableDefaultUI={true}
          >
            {schedules.map((schedule) => {
              if (
                selectedSchedule == null ||
                selectedSchedule === schedule.name
              ) {
                return (
                  <Schedule
                    key={schedule.name}
                    name={schedule.name}
                    vendorLocation={schedule.location}
                    address={schedule.address}
                    appointments={schedule.appointments}
                    color={getRandomColor()}
                  />
                );
              }
            })}
          </Map>
        )}
      </APIProvider>
    </div>
  );
}

export default App;
