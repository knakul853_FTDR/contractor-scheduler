export const getSchedules = () => {
  return fetch("http://localhost:8000/contractor-scheduler/schedules")
    .then((response) => response.json())
    .then((response) => {
      console.log("response", response);
      return response.schedules;
    });

  // return Promise.resolve([
  //   {
  //     name: "Mr John Sinha",
  //     location: {
  //       latitude: 40.7128,
  //       longitude: -74.006,
  //     },
  //     address: "123 Main St, New York, NY 10001, USA",
  //     appointments: [
  //       {
  //         start: "2024-06-10 09:00:00",
  //         end: "2024-06-10 11:00:00",
  //         customerName: "Mr John Smith",
  //         customerLocation: {
  //           latitude: 38.8951,
  //           longitude: -77.0364,
  //         },
  //         CustomerAddress: "132, My Street, Washington, DC 20001, USA",
  //         trades: "heating",
  //         itemName: "Air-conditioner",
  //       },
  //       {
  //         start: "2024-06-10 11:01:00",
  //         end: "2024-06-10 14:00:00",
  //         customerName: "Mr Adams Palmer",
  //         customerLocation: {
  //           latitude: 37.7749,
  //           longitude: -122.4194,
  //         },
  //         CustomerAddress: "132, My Street, San Francisco, CA 94101, USA",
  //         trades: "air-conditioning",
  //         itemName: "AC",
  //       },
  //       {
  //         start: "2024-06-10 15:00:00",
  //         end: "2024-06-10 17:00:00",
  //         customerName: "Jeffrey Jhong",
  //         customerLocation: {
  //           latitude: 41.8781,
  //           longitude: -87.6298,
  //         },
  //         CustomerAddress:
  //           "Jeffrey Jhong, 132, My Street, Chicago, IL 60601, USA",
  //         trades: "heating",
  //         itemName: "Heater",
  //       },
  //     ],
  //   },
  //   // {
  //   //   name: "Mr Johny Depp",
  //   //   location: {
  //   //     latitude: 37.7749,
  //   //     longitude: -122.4194,
  //   //   },
  //   //   address: "789 Pine St, San Francisco, CA 94101, USA",
  //   //   appointments: [
  //   //     {
  //   //       start: "2024-06-10 11:00:00",
  //   //       end: "2024-06-10 14:00:00",
  //   //       customerName: "Mr John Weasly",
  //   //       customerLocation: {
  //   //         latitude: 34.0522,
  //   //         longitude: -118.2437,
  //   //       },
  //   //       CustomerAddress: "132, My Street, Los Angeles, CA 90001, USA",
  //   //       trades: "appliances",
  //   //       itemName: "Dishwasher",
  //   //     },
  //   //   ],
  //   // },
  // ]);
};
