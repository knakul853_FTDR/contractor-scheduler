import React, { useState, useEffect } from "react";
import { useMapsLibrary, useMap } from "@vis.gl/react-google-maps";
import { MarkerWithInfoWindow } from "./Marker";
export const Directions = ({
  origin,
  destination,
  routeInfo,
  color,
  scheduleNo,
}) => {
  const map = useMap();
  const routesLibrary = useMapsLibrary("routes");
  const [directionsService, setDirectionsService] = useState();
  const [directionsRenderer, setDirectionsRenderer] = useState();
  const [directedRendered, setDirectionRendered] = useState(false);

  useEffect(() => {
    if (!routesLibrary || !map) return;
    setDirectionsService(new routesLibrary.DirectionsService());
    setDirectionsRenderer(
      new routesLibrary.DirectionsRenderer({
        map,
        suppressMarkers: true,
        polylineOptions: {
          strokeColor: color,
          strokeWeight: 8,
        },
      })
    );
  }, [routesLibrary, map, color]);

  useEffect(() => {
    if (!directionsService || !directionsRenderer) return;

    directionsService
      .route({
        origin: { lat: origin.lat, lng: origin.lng },
        destination: { lat: destination.lat, lng: destination.lng },
        travelMode: "DRIVING",
        provideRouteAlternatives: true,
      })
      .then((response) => {
        directionsRenderer.setDirections(response);
        setDirectionRendered(true);
      });

    return () => directionsRenderer.setMap(null);
  }, [
    directionsService,
    directionsRenderer,
    origin.lat,
    origin.lng,
    destination.lat,
    destination.lng,
  ]);

  if (!directedRendered) {
    return null;
  }

  const overviewPath = directionsRenderer.directions.routes[0].overview_path;
  const middleIndex = parseInt(overviewPath.length / 2);
  const middlePostion = overviewPath[middleIndex];
  const middleLoc = { lat: middlePostion.lat(), lng: middlePostion.lng() };
  return (
    <>
      {scheduleNo === 1 && (
        <MarkerWithInfoWindow
          position={{ lat: origin.lat, lng: origin.lng }}
          isSource={true}
        >
          <div>
            <div>
              {scheduleNo === 1 ? "Contractor" : "Customer"}: {origin.name}
            </div>
            <div>Address: {origin.address}</div>
            <div>Appointment time: {origin.appointmentTime}</div>
          </div>
        </MarkerWithInfoWindow>
      )}

      <MarkerWithInfoWindow
        position={{ lat: destination.lat, lng: destination.lng }}
      >
        <div>
          <div>Customer: {destination.name}</div>
          <div>Address: {destination.address}</div>
          <div>Appointment time: {destination.appointmentTime}</div>
        </div>
      </MarkerWithInfoWindow>
      <MarkerWithInfoWindow
        position={{ lat: middleLoc.lat, lng: middleLoc.lng }}
        isRouteInfo={true}
      >
        <div className="route-info">
          <div>Contractor Name: {routeInfo.vendorName}</div>
          <div>Schedule Number: {scheduleNo}</div>
          <div>Trade: {routeInfo.trade}</div>
          <div>Item: {routeInfo.item}</div>
          <div>Time: {routeInfo.appointmentTime}</div>
        </div>
      </MarkerWithInfoWindow>
    </>
  );
};
