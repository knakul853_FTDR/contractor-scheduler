import React, { useState } from "react";
import {
  AdvancedMarker,
  InfoWindow,
  useAdvancedMarkerRef,
  Pin,
} from "@vis.gl/react-google-maps";

export const MarkerWithInfoWindow = ({
  position,
  children,
  isRouteInfo,
  isSource,
}) => {
  const [infowindowOpen, setInfowindowOpen] = useState(
    isRouteInfo ? true : false
  );
  const [markerRef, marker] = useAdvancedMarkerRef();

  return (
    <>
      <AdvancedMarker
        ref={markerRef}
        onClick={() => setInfowindowOpen(true)}
        position={position}
        title={"AdvancedMarker that opens an Infowindow when clicked."}
      >
        {isRouteInfo ? (
          <div className="route-info"></div>
        ) : (
          isSource && (
            <Pin
              background={"#008000"}
              glyphColor={"#000"}
              borderColor={"#000"}
            />
          )
        )}
      </AdvancedMarker>
      {infowindowOpen && (
        <div className={isRouteInfo && "route-info-wrapper"}>
          <InfoWindow
            anchor={marker}
            maxWidth={200}
            onCloseClick={() => setInfowindowOpen(false)}
            className={isRouteInfo && "route-info"}
          >
            {children}
          </InfoWindow>
        </div>
      )}
    </>
  );
};
