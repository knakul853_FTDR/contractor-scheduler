import { Directions } from "./Direction";
export const Schedule = ({
  name,
  address,
  vendorLocation,
  appointments,
  color,
}) => {
  console.log("rendoer scheudle");

  const directions = [];

  const initialDirection = {
    origin: {
      name: name,
      lat: vendorLocation.latitude,
      lng: vendorLocation.longitude,
      address: address,
    },
    destination: {
      name: appointments[0].customerName,
      lat: appointments[0].customerLocation.latitude,
      lng: appointments[0].customerLocation.longitude,
      address: appointments[0].customerAddress,
    },
    routeInfo: {
      vendorName: name,
      trade: appointments[0].trades,
      item: appointments[0].itemName,
      appointmentTime: `${appointments[0].start} - ${appointments[0].end}`,
    },
  };

  directions.push(initialDirection);
  for (let i = 1; i < appointments.length; i++) {
    const direction = {
      origin: {
        name: appointments[i - 1].customerName,
        lat: appointments[i - 1].customerLocation.latitude,
        lng: appointments[i - 1].customerLocation.longitude,
        address: appointments[i - 1].CustomerAddress,
      },
      destination: {
        name: appointments[i].customerName,
        lat: appointments[i].customerLocation.latitude,
        lng: appointments[i].customerLocation.longitude,
        address: appointments[i].CustomerAddress,
      },
      routeInfo: {
        vendorName: name,
        trade: appointments[i].trades,
        item: appointments[i].itemName,
        appointmentTime: `${appointments[i].start} - ${appointments[i].end}`,
      },
    };
    directions.push(direction);
  }
  console.log("directions:", directions);
  return (
    <>
      {directions.map((direction, index) => {
        return (
          <Directions
            key={`${color}-${direction.origin.lat}-${direction.origin.lng}`}
            origin={direction.origin}
            destination={direction.destination}
            routeInfo={direction.routeInfo}
            color={color}
            scheduleNo={index + 1}
          />
        );
      })}
    </>
  );
};
